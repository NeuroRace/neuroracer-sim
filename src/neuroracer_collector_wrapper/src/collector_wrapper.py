#!/usr/bin/env python
from neuroracer_common.run_wrapper import run_collector


def main():
    run_collector()


if __name__ == '__main__':
    main()
