cmake_minimum_required(VERSION 2.8.3)
project(neuroracer_engine_wrapper)

## Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS rospy geometry_msgs sensor_msgs std_msgs)

include_directories(${catkin_INCLUDE_DIRS})

catkin_package(
  INCLUDE_DIRS
  CATKIN_DEPENDS rospy geometry_msgs sensor_msgs std_msgs message_runtime
  DEPENDS
)

install(DIRECTORY launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)
