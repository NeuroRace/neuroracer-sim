#!/usr/bin/env python

from __future__ import division
from __future__ import print_function

import rospy
from ackermann_msgs.msg import AckermannDriveStamped
from geometry_msgs.msg import TwistStamped
from neuroracer_configclient import ConfigClient

VESC_INPUT_TOPIC = "/vesc/high_level/ackermann_cmd_mux/input/default"


class EngineWrapper:
    """
    A class to simulate an engine adaption of the robot.
    Also transforming stamped twist_msgs into stamped ackermann_msgs
    required by the simulation.

    Necessary to keep the Ackermann dependency in simulation only.
    """

    def __init__(self, engine_input_publisher, speed_scale, publish_rate):
        self.engine_input_publisher = engine_input_publisher
        self.publish_rate = publish_rate

        # speed values' range is from -1 to 1. Simulation sets directly in m/s.
        # So using 5 as scale factor for example, will result in a maximum speed of 5 m/s.
        self.speed_scale = speed_scale

        self.action = None

        # used to transform the msgs for vesc
        self.engine_input_transform_publisher = None

    @staticmethod
    def create(config):
        """
        Factory method to create a EngineWrapper instance based on a given config
        dict (usually from the config server).

        :param config: config dict
        :type config: Dict[str, Dict]
        :return: new EngineWrapper instance
        :rtype: EngineWrapper
        """

        rospy.init_node("engine_wrapper")
        publish_rate = rospy.Rate(config["command_publish_rate"])

        engine_input_publisher = rospy.Publisher(name=config["output_topic"],
                                                 data_class=AckermannDriveStamped,
                                                 queue_size=1)

        engine_wrapper = EngineWrapper(engine_input_publisher=engine_input_publisher,
                                       speed_scale=config["speed_scale"],
                                       publish_rate=publish_rate)

        rospy.Subscriber(name=config["input_topic"],
                         data_class=TwistStamped,
                         callback=engine_wrapper._callback_twist_to_ackermann)

        # used to transform the msgs for vesc
        engine_wrapper.engine_input_transform_publisher = rospy.Publisher(name=VESC_INPUT_TOPIC,
                                                                          data_class=AckermannDriveStamped,
                                                                          queue_size=1)
        # used to transform the msgs for vesc
        rospy.Subscriber(name=config["output_topic"],
                         data_class=AckermannDriveStamped,
                         callback=engine_wrapper._callback_transform_action_for_engine)

        return engine_wrapper

    @staticmethod
    def from_configserver(config_path):
        """
        Factory method to create a EngineWrapper instance with a config from the
        configserver.

        :param config_path: configserver path to the config entry
        :type config_path: str
        :return: new EngineWrapper instance
        :rtype: EngineWrapper
        """

        config_client = ConfigClient()
        config = config_client.get(path=config_path)

        return EngineWrapper.create(config)

    def _callback_twist_to_ackermann(self, twist_msg):
        """
        Internal callback function for the ros joy node.

        :param twist_msg: ros twist msg
        :type twist_msg: geometry_msgs.msg.TwistStamped
        """
        ackermann_msg = AckermannDriveStamped()
        # TODO Unix timestamp here?
        ackermann_msg.header = twist_msg.header
        ackermann_msg.drive.steering_angle = twist_msg.twist.angular.z  # steering
        ackermann_msg.drive.speed = twist_msg.twist.linear.x * self.speed_scale  # speed

        self.action = ackermann_msg

    def _callback_transform_action_for_engine(self, ackermann_msg):
        """
        Internal callback function for the engine_wrapper output node
        transforms the ackermann msgs for simulation and published it

        :param ackermann_msg: ros ackermann msg
        :type ackermann_msg: ackermann_msgs.msg.AckermannDriveStamped
        """
        ackermann_msg.drive.steering_angle = -ackermann_msg.drive.steering_angle
        self.engine_input_transform_publisher.publish(ackermann_msg)

    def run(self):
        """
        Lets the EngineWrapper spin until a ros shutdown happens.
        """
        while not rospy.is_shutdown():
            if not (self.action is None):
                self.engine_input_publisher.publish(self.action)

            self.publish_rate.sleep()


def main():
    try:
        engine_wrapper = EngineWrapper.from_configserver("engine_wrapper")
        engine_wrapper.run()
    except rospy.ROSInterruptException:
        pass


if __name__ == '__main__':
    main()
