# Simulation

In this repository you will find the [catkin workspace](http://wiki.ros.org/catkin/workspaces) used to run the simulation.
Catkin is the building tool for [ROS Projects](http://wiki.ros.org/Documentation) and ROS & [Gazebo](http://gazebosim.org/)
are the tools which were used to create this simulation. If you follow the instructions under [Setup](#setup) you will be able
to execute the launch command, run the simulation and control it with a controller. 

The following describes the execution workflow to run the simulation. First you need to execute the `roslaunch` command in
a terminal, your controller should already be connected with your system.

The [roslaunch](http://wiki.ros.org/roslaunch) command is the following:

```bash
roslaunch racecar_gazebo neuroracer_sim.launch
```

After the launch command gazebo should popup and you should be able to control the simulation with your controller. This 
is displayed in the following image.

![simulation run workflow](images/sim_run_flow.png)
        
## Content
- [Requirements](#requirements)
- [Python Version](#python-version)
- [Setup](#setup)
  - [Software](#software)
  - [Launch File](#launch-file)
  - [Config Directory](#config-directory)
  - [Gazebo World](#gazebo-world)
  - [Manual Control](#manual-control)
- [Architecture](#architecture)
- [MIT-Racecar](#mit-racecar)
- [Media](#media)

## Requirements
* __ros-melodic-desktop-full__  
The included desktop-full's linux packages vary on some distributions, a full list of all used packages can be found [here](ros_packages_list.txt).

* __neuroracer-common__  
The project [__neuroracer-common__](https://gitlab.com/NeuroRace/neuroracer-common.git) is needed to execute the simulation.

* __neuroracer-ai__ (optional)  
The project [__neuroracer-ai__](https://gitlab.com/NeuroRace/neuroracer-ai.git) is optional and only needed for autonomous 
tasks.   

## Python Version

Due to [ROS](http://wiki.ros.org/Documentation), Python 2.7 is required. As soon as ROS fully supports Python3.x, the Python2.7 support will be dropped.

## Setup 

### Software
Clone this repository and execute [`setupWorkspace.sh`](setupWorkspace.sh).

```bash
bash setupWorkspace.sh
```

To use this catkin workspace it is necessary to install [`neuroracer-common`](https://gitlab.com/NeuroRace/neuroracer-common.git) 
package via pip. Therefor clone and install the package. You don't have to and shouldn't clone neuroracer-common into the
neuroracer-sim directory.  
```bash
git clone https://gitlab.com/NeuroRace/neuroracer-common.git
pip2 install --user -e neuroracer-common
```

For autonomous tasks, the package [`neuroracer-ai`](https://gitlab.com/NeuroRace/neuroracer-ai.git) is required. Also you don't have 
to and shouldn't clone neuroracer-ai into the neuroracer-sim directory.  
```bash
git clone https://gitlab.com/NeuroRace/neuroracer-ai.git
pip2 install --user -e neuroracer-ai
```

Further, the [`neuroracer-configserver`](https://gitlab.com/NeuroRace/neuroracer-configserver.git) is used for setup and 
configuration handling. Clone the repository into [`src`](src/) folder and install the client.
```bash
cd src
git clone https://gitlab.com/NeuroRace/neuroracer-configserver.git
cd neuroracer-configserver
python2 setup.py install --user
```

Now build the catkin workspace, you need to be in the [`neuroracer-sim`](https://gitlab.com/NeuroRace/neuroracer-sim.git) directory. 
The **p**rint **w**orking **d**irectory command should display something like the following:
```bash
pwd
/home/user/NeuroRace-Git/neuroracer-sim
```

The current working directory is __neuroracer-sim/__, here execute the following catkin build command to build the workspace:
```bash
catkin_make
```

### Launch File
The default launch file location is __neuroracer-sim/src/racecar-simulator/racecar_gazebo/launch/neuroracer_sim.launch__, 
this file gets created after [`setupWorkspace.sh`](setupWorkspace.sh) was executed.

* The default directory for the configuration files is __~/nr_configs/__
* The configuration files directory can be customized by editing the following entry in __neuroracer_sim.launch__:  
``<arg name="config_dir" default="~/nr_configs"/>``

To use ai in the simulation you have to activate the ai_wrapper_node in the launch file.
Only activate the ai if you already have a trained model. If the ai_wrapper can not find 
or load a model, it will crash. If you want to use the ai_wrapper_node the launch file 
should look like this:

```
<!-- AI-Wrapper -->
<!-- only activate with proper model prepaired to load --> 
<node name="neuroracer_ai_wrapper" pkg="neuroracer_ai_wrapper" type="ai_wrapper.py" output="screen" />
```

### Config Directory
The configuration folder requires the following files, [example files can be found here](example_configs):
```
nr_configs/
|---> ai_wrapper.yaml (optional - only required when AI-Wrapper is activated in launch file)
|---> collector.yaml
|---> engine_wrapper.yaml
|---> joy_mapping.yaml
\---> operator.yaml
```

* __ai_wrapper.yaml__: Configuration file for the ai node. Contains the needed information for autonomous driving like 
camera topic, model directory or model name.
* __collector.yaml__: Configuration file for the recording node. Contains the topics which should be recorded and the 
save directory.
* __engine_wrapper.yaml__: Configuration file for the engine node. Contains the engine topics or the maximum speed.
* __joy_mapping.yaml__: Configuration file for the controller button mapping.
* __operator.yaml__: Configuration file for the control operator node. Central operator for ai and controller commands.

### Gazebo World
Example for modified track from MIT: [ZIP here](tracks/modified_track.zip)
* World: __mit_modified_racecourse.world__ has to be copied to _neuroracer-sim/src/racecar-simulator/racecar_gazebo/worlds/_

* Models: copy the whole __mit_modified_racecourse__ directory from the archive into _$HOME/.gazebo/models/_ (__don't use__ _neuroracer-sim/src/racecar-simulator/racecar_description/models/_ for the model files).

* Launch file: __neuroracer_sim.launch__ in _neuroracer-sim/src/racecar-simulator/racecar_gazebo/launch/_
is the used launch file

  * the argument __world_name__ has to be set to _mit_modified_racecourse_ example:  
  ``<arg name="world_name" default="mit_modified_racecourse"/>``
  * the argument __use_sim_time__ should default be set to _true_ example:  
  ``<arg name="use_sim_time" default="true"/>``

The following step has to be executed afterwards (only once):
* Start Gazebo with the following command
```bash
gazebo
```

* After the successful start, Gazebo can be closed (this step is necessary, so the world is added to Gazebo 
internally, but as said it has to be done only once)

* Now run the default neurorace launch file
```bash
roslaunch racecar_gazebo neuroracer_sim.launch
```

### Manual Control
The controller mapping is defined in __joy_mapping.yaml__. Examples can be found [here](example_configs/joy_mapping_examples).  

__IMPORTANT:__  
The yaml file for joy mapping has to be named __joy_mapping.yaml__ (not joy_mapping_xbox360.yaml or otherwise). To steer 
or accelerate the simulation car the __deadman__ button has to be hold down. The button mapping is displayed in the 
following image.

![alt Default Controller Layout](https://gitlab.com/NeuroRace/neuroracer-robot/raw/master/images/xbox_one_controller_layout.jpg)

## Architecture
![Imitation Learning Architecture](images/neuroracer-sim-architecture.png)

## MIT-Racecar
The `neuroracer-sim` is build atop of the MIT-Racecar and simulation environment, which is a project of the Massachusetts Institute of Technology (Cambridge, USA), providing a basic racecar model and environment. 
The external repositories are differently licensed and not maintained by the NeuroRace team. Some parts have been and others will be modified during future development. See the `patches` and `extensions` sections in [`setupWorkspace.sh`](setupWorkspace.sh) for further details. 

* __ai_wrapper.yaml__: Configuration file for the ai node. Contains the needed information for autonomous driving like 
camera topic, model directory or model name.
* __collector.yaml__: Configuration file for the recording node. Contains the topics which should be recorded and the 
save directory.
* __engine_wrapper.yaml__: Configuration file for the engine node. Contains the engine topics or the maximum speed.
* __joy_mapping.yaml__: Configuration file for the controller button mapping.
* __operator.yaml__: Configuration file for the control operator node. Central operator for ai and controller commands.

## Media
### SL Constant Speed - Simulation (2 m/s)
![](http://home.htw-berlin.de/~baumapa/ai_track_overview.mp4)
![](http://home.htw-berlin.de/~baumapa/ai_driver_perspective_color.mp4)
